<?php

$fh = fopen("Studenty.txt", "rt");

if (!$fh) {
	die("�� ���� ��������� ����");
}

$mestnye = array();
$inogorodnye = array();
$list = array();

fgets($fh); // � ������ ������ ��������� �������, ���������

// ������� ����� �������
$row = fgets($fh);
$head = explode("\t", $row);

while($row = fgets($fh)) {
	$cols = explode("\t", $row); // ������� ������ �� ����������

	if ($cols[3] == "�-���������") {
		// �������
		$mestnye[] = $cols;
	} else {
		// �����������
		$inogorodnye[] = $cols;
	}

	$list[] = $cols; // �������� ��� ����������� ������ � ����
}

fclose($fh); // ������� �������� ����

echo "<h1>������ ����������� ���������</h1>";
echo "<table>";
echo "<thead>";
echo "<tr><th>", join("</th><th>", $head), "</th></tr>";
echo "</thead>";
echo "<tbody>";
// ������� ������ ����������� ���������
foreach($inogorodnye as $cols) {
	echo "<tr><td>", join("</td><td>", $cols), "</td></tr>";
}
echo "</tbody>";
echo "</table>";

echo "<h1>������� ��������</h1>";
echo "<table>";
echo "<thead>";
unset($head[3]); // � ������� �� ���������� ����� � �����
echo "<tr><th>", join("</th><th>", $head), "</th></tr>";
echo "</thead>";
echo "<tbody>";
// ������� ������ ������� ���������
foreach($mestnye as $cols) {
	unset($cols[3]); // ����� �������� ����� � ������
	echo "<tr><td>", join("</td><td>", $cols), "</td></tr>";
}
echo "</tbody>";
echo "</table>";

// �������� � ����
$fout = fopen("stydenty-out.txt", "wt"); // ������� ���� ��� ������
// ������� ������� "�����"
array_unshift($head, "����� �/�"); // � ������ �������
// �������� � ����
fputs($fout, join("\t", $head)); // ������ ������� ����� ����������� � ������� � ����

foreach($list as $k => $cols) {
	// $k - ���� �������, ���������� � 0
	array_unshift($cols, $k+1); // ������� ����� ������ ��������
	fputs($fout, join("\t", $cols));
}

fclose($fout); // ������� ���� ��� ������

?>