<?php

$chisla = array();

$fh = fopen("chisla.txt", "rt");

if (!$fh) {
	die("�� ���� ��������� ����");
}

while($ch = fgets($fh)) {
	$chisla[] = trim($ch);
}

fclose($fh);

$firstSum = sum($chisla, 1, count($chisla));

$secondSum = sum(getPow($chisla, 2), 2, count($chisla));

$thirdSum = sum(getPow($chisla, 0.5), 1, count($chisla)-2);

$fourthSum = sum(getPow($chisla, 2/3), 3, count($chisla));


echo ($firstSum + $secondSum) / ($thirdSum + $fourthSum);


// functions
function getPow($list, $pow) {
	$result = array();
	foreach($list as $x) {
		$result[] = pow($x, $pow);
	}

	return $result;
}

function sum($list, $from, $to) {
	$result = 0;
	// ������ �������� ���������� � 0
	// �.�. � ������� �������� ������ 0, �.�. 1-1
	// ������� ������� � $from-1
	// c $to ����������
	for($i = ($from-1); $i <= ($to-1); $i++) {
		$result += $list[ $i ];
	}

	return $result;
}