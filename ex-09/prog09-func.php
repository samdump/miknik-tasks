<?php

function perevod($per_v_sec) {
	$days = (int) ( $per_v_sec / (60 * 60 * 24) );
	$hours = (int) ( ($per_v_sec - $days * 60 * 60 * 24) / (60 * 60) );
	$minutes = (int) ( ($per_v_sec - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60 );
	$seconds = $per_v_sec - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60;

	return array($days, $hours, $minutes, $seconds);
}